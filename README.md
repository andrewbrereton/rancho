# Rancho

A RancherOS config for my Mini PC.

## Reading Material

https://rancher.com/docs/os/v1.x/en/running-rancheros/workstation/docker-machine/

## Install

### Testing on weird Chinese mini PC thing

#### Hardware

Mini PC J1900 Quad Core 4G RAM Windows 10 Dual LAN 2 COM Fanless Mini Computer 3205U J1800 N2810 Nottop with 300M WIFI HDMI VGA

https://www.aliexpress.com/item/Mini-PC-J1900-Quad-Core-4G-RAM-Windows-10-Dual-LAN-2-COM-Fanless-Mini-Computer/32839489621.html

#### Bootable USB

Download latest stable ISO file:

https://github.com/rancher/os/releases/

Bootable USB stick from ISO on OSX:

https://blog.tinned-software.net/create-bootable-usb-stick-from-iso-in-mac-os-x/

#### Do it

Plug in keyboard, screen, ethernet cable and USB drive. Start device. Press F11. Boot from USB.

Once sent to command prompt:

```shell
sudo ros install --device /dev/sda --cloud-config https://gitlab.com/mustardshrimp/rancho/raw/master/cloud-config.yml
```

When asked to continue, say yes

When asked to continue with reboot, say yes

Remove USB drive.

Get IP address from Rancher loaded screen.

Now you can login from any machine on the network where their SSH public key was specified in the cloud-config.yml:

```shell
ssh rancher@192.168.1.200
```

Set console

```shell    
sudo ros console switch ubuntu
```

SSH back in after that.

Now you can remove keyboard and screen from device.

Install basic admin tool:

```shell
sudo system-docker run -d --net=host --name busydash husseingalal/busydash
# to make it start at boot time
sudo mkdir -p /opt/rancher/bin
echo "sudo system-docker start busydash" | sudo tee -a /opt/rancher/bin/start.sh
sudo chmod 755 /opt/rancher/bin/start.sh
```

Navigate to http://192.168.0.13 to see basic admin tool.

#### Updating cloud-config.yml with live system

Make live changes to cloud-config.yml. Save. Commit to repo. Then:

```shell
curl --silent https://gitlab.com/mustardshrimp/rancho/raw/master/cloud-config.yml > /tmp/cloud-config.yml && \
    sudo ros config validate -i /tmp/cloud-config.yml

sudo mv /tmp/cloud-config.yml /var/lib/rancher/conf/cloud-config.yml &&
    sudo reboot -r
```

#### Reset Docker

Sometimes you can make changes to cloud-config.yml but you don't see the changes reflected in the containers. In this case it can be useful to hard reset Docker and start again.

This will stop all containers, remove all containers and remove all images.

```shell
docker stop $(docker ps -a -q) && \
    docker rm $(docker ps -a -q) && \
    docker rmi $(docker images -q)
```

Now reboot the host so that it can download, build and start all the containers from scratch. The beauty is the cloud-config.yml tells Rancher what to build, and the NFS store has all of the data.

#### Bricked It

This can happen if the cloud-config.yml is invalid.

First of all, fix the cloud-config.yml, commit and push to repo.

Plug in USB

Boot from USB into command-line

```shell
# re-image!
sudo ros install \
    --device /dev/sda \
    --cloud-config https://gitlab.com/mustardshrimp/rancho/raw/master/cloud-config.yml \
    --force
```

#### Upgrade RancherOS

SSH into the machine and run

```shell
# list versions
sudo ros os list

# upgrade to latest
sudo ros os upgrade
```

----

### Testing on dev machine using `docker-machine`

Install virtualbox with RancherOS in it:

```shell
docker-machine create \
    --driver virtualbox \
    --virtualbox-memory "1024" \
    --virtualbox-cpu-count "1" \
    --virtualbox-disk-size "61440" \
    --virtualbox-share-folder `pwd`:rancho \
    --virtualbox-boot2docker-url https://releases.rancher.com/os/latest/rancheros.iso rancho
```

Check to see if it worked:

```shell
VBoxManage list runningvms | grep rancho
```

Login:

```shell
docker-machine ssh rancho
```

Configure network:

```shell
sudo ros config set rancher.network.interfaces.eth1.address 192.168.99.10/24
sudo ros config set rancher.network.interfaces.eth1.dhcp false
sudo reboot
```

After reboot, login:

```shell
docker-machine ssh rancho
```

Regenerate certificates

```
docker-machine regenerate-certs rancho -f
```

To run all `docker` commands within this docker machine:

```shell
eval $(docker-machine env rancho)
```

Now when I run docker commands from my shell, they will be executed within this docker machine.

## Configure RancherOS

```
eval $(docker-machine env rancho)
docker volume create downloads
```

## Configure containers

Here are the steps used to create each container.

### qBittorrent 

```
eval $(docker-machine env rancho)
docker create \
    --name=qbittorrent \
    -v config:/config \
    -v downloads:/downloads \
    -e PGID=1100 \
    -e PUID=1100 \
    -e UMASK_SET=022 \
    -e WEBUI_PORT=8080 \
    -e TZ=Melbourne\Australia \
    -p 6881:6881 \
    -p 6881:6881/udp \
    -p 8080:8080 \
    --restart=unless-stopped \
    linuxserver/qbittorrent
docker start qbittorrent
```

### Syncthing

```
eval $(docker-machine env rancho)
docker create \
    --name=syncthing \
    -v config:/config \
    -v data:/mnt/blah/documents \
    -v data:/mnt/blah/passwords \
    -v data:/mnt/blah/photos \
    -e PGID=1100 \
    -e PUID=1100 \
    -e UMASK_SET=022 \
    -p 8384:8384 \
    -p 22000:22000 \
    -p 21027:21027/udp \
    --restart=unless-stopped \
    linuxserver/syncthing
docker start syncthing
```

### PrivateInternetAccess

```
eval $(docker-machine env rancho)
docker create \
    --name=pia \
    --cap-add=NET_ADMIN \
    --device=/dev/net/tun \
    --dns 209.222.18.222 \
    --dns 209.222.18.218 \
    -e 'REGION=AU Melbourne' \
    -v 'auth.conf:auth.conf' \
    colinhebert/pia-openvpn \
    --auth-user-pass auth.conf
docker start pia --auth-user-pass auth.conf
```


## Start Over

```shell
docker stop $(docker ps -a -q) && \ 
    docker rm $(docker ps -a -q) && \
    docker rmi $(docker images -q)
```